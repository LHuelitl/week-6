﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody rb;
    public string HorzAxis = "Horizontal";
    public string VertAxis = "Vertical";
    public float MaxSpeed = 5f;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();

    }

    void FixedUpdate()
    {
        //Update movement
        float Horz = Input.GetAxis(HorzAxis);
        float Vert = Input.GetAxis(VertAxis);
        Vector3 MoveDirection = new Vector3(Horz, 0.0f, Vert);
        rb.AddForce(MoveDirection.normalized * MaxSpeed);
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "ladder")
            if (Input.GetButtonDown ("Jump"))
            {
                rb.AddForce(new Vector3(0, 100, 0));
            }
    }
}